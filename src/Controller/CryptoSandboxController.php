<?php
/**
 * @file
 * Contains \Drupal\example\Controller\ExampleController.
 */
namespace Drupal\crypto_sandbox\Controller;
use Drupal\Core\Controller\ControllerBase;

class CryptoSandboxController {
	public function start() {
		return [
			'#markup' => '<h2>Welcome to the start page</h2>',
			$this->basic_form()
		];
	}

	public function basic_form (){

		$form['#attached']['library'][] = "crypto_sandbox/openpgp";

		return $form;
	}

}
